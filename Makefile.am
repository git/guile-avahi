# Guile-Avahi --- Guile bindings for Avahi.
# Copyright (C) 2007, 2023 Ludovic Courtès <ludo@gnu.org>
#
# This file is part of Guile-Avahi.
#
# Guile-Avahi is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# Guile-Avahi is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

AUTOMAKE_OPTIONS = foreign
ACLOCAL_AMFLAGS = -I m4

EXTRA_DIST =            \
  README		\
  pre-inst-guile.in

GUILE_FOR_BUILD =						\
  GUILE_AUTO_COMPILE=0						\
  $(GUILE) -L $(top_srcdir)/modules -L $(top_builddir)/modules


#
# C code.
#

noinst_HEADERS = src/watch.h src/utils.h src/errors.h

EXTRA_DIST +=								\
  src/make-enum-map.scm src/make-smob-types.scm				\
  src/make-enum-header.scm src/make-smob-header.scm			\
  src/make-callback-header.scm src/make-callback-trampolines.scm

# Files generated by (avahi build ...).
GENERATED_BINDINGS =				\
   src/common-smobs.h src/common-enums.h	\
   src/client-smobs.h src/client-enums.h	\
   src/client-callbacks.h			\
   src/publish-smobs.h src/publish-enums.h	\
   src/publish-callbacks.h			\
   src/lookup-smobs.h src/lookup-enums.h	\
   src/lookup-callbacks.h			\
   src/common-enums.i.c src/common-smobs.i.c	\
   src/client-enums.i.c src/client-smobs.i.c	\
   src/client-callbacks.i.c			\
   src/publish-enums.i.c src/publish-smobs.i.c	\
   src/publish-callbacks.i.c			\
   src/lookup-enums.i.c src/lookup-smobs.i.c	\
   src/lookup-callbacks.i.c

BUILT_SOURCES =						\
   $(GENERATED_BINDINGS)				\
   src/common.c.x src/watch.c.x src/client.c.x		\
   src/errors.c.x src/publish.c.x src/lookup.c.x

nobase_guileextension_LTLIBRARIES  = guile-avahi-v-0.la

# Use '-module' to build a "dlopenable module", in Libtool terms.
# Use '-undefined' to placate Libtool on Windows; see
# <https://lists.avahi.org/pipermail/avahi-devel/2014-December/007294.html>.
guile_avahi_v_0_la_LDFLAGS = -module -no-undefined

guile_avahi_v_0_la_SOURCES =			\
  src/utils.c src/errors.c src/watch.c		\
  src/common.c src/client.c			\
  src/publish.c src/lookup.c
guile_avahi_v_0_la_CFLAGS =			\
  $(AVAHI_CFLAGS) $(GUILE_CFLAGS) $(AM_CFLAGS)
guile_avahi_v_0_la_LIBADD =			\
   $(AVAHI_LIBS) $(GUILE_LDFLAGS)

AM_CFLAGS   = $(GCC_CFLAGS)
AM_CPPFLAGS = -I$(builddir)/src -I$(srcdir)/src			\
	      -I$(top_srcdir)/lib -I$(top_builddir)/lib


%-enums.h: $(srcdir)/src/make-enum-header.scm modules/avahi/build/config.scm
	$(AM_V_GEN)$(GUILE_FOR_BUILD) $< $$(echo $$(basename $@) | cut -f 1 -d -) > $@

%-smobs.h: $(srcdir)/src/make-smob-header.scm modules/avahi/build/config.scm
	$(AM_V_GEN)$(GUILE_FOR_BUILD) $< $$(echo $$(basename $@) | cut -f 1 -d -) > $@

%-enums.i.c: $(srcdir)/src/make-enum-map.scm modules/avahi/build/config.scm
	$(AM_V_GEN)$(GUILE_FOR_BUILD) $< $$(echo $$(basename $@) | cut -f 1 -d -) > $@

%-smobs.i.c: $(srcdir)/src/make-smob-types.scm modules/avahi/build/config.scm
	$(AM_V_GEN)$(GUILE_FOR_BUILD) $< $$(echo $$(basename $@) | cut -f 1 -d -) > $@

%-callbacks.h: $(srcdir)/src/make-callback-header.scm modules/avahi/build/config.scm
	$(AM_V_GEN)$(GUILE_FOR_BUILD) $< $$(echo $$(basename $@) | cut -f 1 -d -) > $@

%-callbacks.i.c: $(srcdir)/src/make-callback-trampolines.scm modules/avahi/build/config.scm
	$(AM_V_GEN)$(GUILE_FOR_BUILD) $< $$(echo $$(basename $@) | cut -f 1 -d -) > $@


# C file snarfing.

snarfcppopts = $(DEFS) $(INCLUDES) $(AM_CPPFLAGS) $(CPPFLAGS)	\
	       $(CFLAGS) $(AM_CFLAGS) $(GUILE_CFLAGS)		\
	       $(AVAHI_CPPFLAGS)

SUFFIXES = .x

%.c.x: %.c $(GENERATED_BINDINGS)
	$(AM_V_GEN)$(guile_snarf) -o $@ $< $(snarfcppopts)


#
# Modules.
#

guilemodulesubdir    = $(guilemoduledir)/avahi
guileclientmoduledir = $(guilemodulesubdir)/client

nodist_guilemodule_DATA =			\
  modules/avahi.scm
dist_guilemodulesub_DATA =			\
  modules/avahi/client.scm
dist_guileclientmodule_DATA =			\
  modules/avahi/client/lookup.scm		\
  modules/avahi/client/publish.scm

documentation_modules =				\
  modules/system/documentation/README		\
  modules/system/documentation/c-snarf.scm	\
  modules/system/documentation/output.scm

EXTRA_DIST +=								\
  modules/avahi/build/enums.scm modules/avahi/build/smobs.scm		\
  modules/avahi/build/callbacks.scm modules/avahi/build/utils.scm	\
  modules/avahi/test.scm						\
  $(documentation_modules)						\
  modules/avahi.in

BUILT_SOURCES +=				\
  modules/avahi/build/config.scm		\
  modules/avahi.scm

.in.scm:
	$(AM_V_GEN)$(MKDIR_P) "`dirname "$@"`" ; cat "$^" |		\
	  $(SED) -e's|[@]maybe_guileextensiondir[@]|$(maybe_guileextensiondir)|g' \
	  > "$@.tmp"
	$(AM_V_at)mv "$@.tmp" "$@"

if HAVE_AVAHI_ERR_NO_CHANGE
modules/avahi/build/config.scm:
	-mkdir -p "`dirname $@`"
	echo '(define-module (avahi build config))' > "$@" && \
	echo '(define-public %have-err-no-change? (= 0 0))' >> "$@"
else
modules/avahi/build/config.scm:
	-mkdir -p "`dirname $@`"
	echo '(define-module (avahi build config))' > "$@" && \
	echo '(define-public %have-err-no-change? (= 0 1))' >> "$@"
endif

CLEANFILES = $(BUILT_SOURCES)


#
# Compiling .scm files.
#

godir = $(libdir)/guile/$(GUILE_EFFECTIVE_VERSION)/site-ccache
gosubdir = $(godir)/avahi
goclientdir = $(gosubdir)/client

nodist_go_DATA =				\
  modules/avahi.go
nodist_gosub_DATA =				\
  modules/avahi/client.go
nodist_goclient_DATA =				\
  modules/avahi/client/lookup.go		\
  modules/avahi/client/publish.go

GOBJECTS = $(nodist_go_DATA) $(nodist_gosub_DATA) $(nodist_goclient_DATA)

AM_V_GUILEC = $(AM_V_GUILEC_$(V))
AM_V_GUILEC_ = $(AM_V_GUILEC_$(AM_DEFAULT_VERBOSITY))
AM_V_GUILEC_0 = @echo "  GUILEC  " $@;

if CROSS_COMPILING
CROSS_COMPILING_VARIABLE = AVAHI_GUILE_CROSS_COMPILING=yes
else
CROSS_COMPILING_VARIABLE =
endif

# Make sure 'gnutls.scm' is built first.
# Unset 'GUILE_LOAD_COMPILED_PATH' so we can be sure that any .go file that we
# load comes from the build directory.
# XXX: Use the C locale for when Guile lacks
# <https://git.sv.gnu.org/cgit/guile.git/commit/?h=stable-2.0&id=e2c6bf3866d1186c60bacfbd4fe5037087ee5e3f>.
%.go: %.scm modules/avahi.scm guile-avahi-v-0.la
	$(AM_V_GUILEC)$(MKDIR_P) "`dirname "$@"`" ;		\
	$(AM_V_P) && out=1 || out=- ;				\
	unset GUILE_LOAD_COMPILED_PATH ; LC_ALL=C		\
	GUILE_AUTO_COMPILE=0 $(CROSS_COMPILING_VARIABLE)	\
	AVAHI_GUILE_EXTENSION_DIRECTORY="$(abs_top_builddir)"	\
	$(GUILD) compile --target="$(host)"			\
	  -L "$(top_builddir)/modules"				\
	  -L "$(top_srcdir)/modules"				\
	  -Wformat -Wunbound-variable -Warity-mismatch		\
	  -o "$@" "$<" >&$$out

SUFFIXES += .go

CLEANFILES += $(GOBJECTS)


#
# Tests.
#

SCM_TESTS =								\
  tests/simple-poll.scm tests/threaded-poll.scm tests/publish.scm	\
  tests/publish+browse.scm tests/publish+resolve.scm			\
  tests/guile-poll.scm tests/errors.scm

TESTS = $(SCM_TESTS)
TEST_EXTENSIONS = .scm

AM_TESTS_ENVIRONMENT =							\
  abs_top_srcdir="$(abs_top_srcdir)"; export abs_top_srcdir;		\
  abs_top_builddir="$(abs_top_builddir)"; export abs_top_builddir;	\
  ORIGTERM=${TERM}; export ORIGTERM;					\
  TERM=xterm; export TERM;						\
  GUILE_WARN_DEPRECATED=no; export GUILE_WARN_DEPRECATED;		\
  GUILE_AUTO_COMPILE=0; export GUILE_AUTO_COMPILE;

SCM_LOG_COMPILER = $(abs_top_builddir)/pre-inst-guile
AM_SCM_LOG_FLAGS = -s

EXTRA_DIST += $(TESTS)


#
# Documentation.
#

SNARF_CPPFLAGS =				\
  -I$(top_srcdir)/src -I$(top_builddir)/src	\
  $(GUILE_CFLAGS) $(AVAHI_CPPFLAGS) $(CPPFLAGS)	\
  $(AM_CPPFLAGS)

BUILT_SOURCES +=					\
  doc/common.c.texi doc/watch.c.texi doc/client.c.texi	\
  doc/publish.c.texi doc/lookup.c.texi

info_TEXINFOS = doc/guile-avahi.texi
EXTRA_DIST   += doc/extract-c-doc.scm

# Tell 'makeinfo' where to find the generated .texi files.
AM_MAKEINFOFLAGS =						\
  -I "$(abs_top_builddir)/doc" -I "$(abs_top_srcdir)/doc"

# Likewise for 'texi2dvi'.
TEXI2DVI = texi2dvi $(AM_MAKEINFOFLAGS)

doc/%.c.texi: $(top_srcdir)/src/%.c $(GENERATED_BINDINGS)
	$(AM_V_at)mkdir -p "`dirname $@`"
	$(AM_V_GEN)$(GUILE_FOR_BUILD)			\
	   -l "$(top_srcdir)/doc/extract-c-doc.scm"	\
	   -e '(apply main (cdr (command-line)))'	\
	   -- "$<" "$(CPP)" "$(SNARF_CPPFLAGS)"		\
	   > "$@.tmp"
	$(AM_V_at)mv "$@.tmp" "$@"
